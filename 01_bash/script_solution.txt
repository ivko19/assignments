#!/bin/bash

## Set the search string and replacement string
string1="dev"
string2="prod"
string3="*dev*"
string4="*prod*"

# Find files containing string1 and loop through them
for file in $(find  /home/ivko/testera/ -name "$string3"); do
    # Rename the file by replacing string1 with string2
        new_name=$(echo "$file" | sed "s/$string1/$string2/g")
            mv "$file" "$new_name"
                echo "Renamed $file to $new_name"
                done
