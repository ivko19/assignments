1. [x] Podici postgres docker image
2. [x] Kreirati aplikativnog korisnika
3. [/] Izbildovati image servisa django_blog
4. [ ] Podici container servisa i konektovati ga na bazu (odvojena mreza ili host mreza)
5. [ ] Podici dva container-a (k8s ili docker compose) i pokazati redudantnost.